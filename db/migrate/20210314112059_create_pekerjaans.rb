class CreatePekerjaans < ActiveRecord::Migration[6.1]
  def change
    create_table :pekerjaans do |t|
      t.string :kode_pekerjaan
      t.string :nama_pekerjaan
      t.timestamps
    end
  end
end
