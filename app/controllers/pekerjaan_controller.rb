class PekerjaanController < ApplicationController
    skip_before_action :verify_authenticity_token
    #====================================== method tampil data pekerjaan keseluruhan
    def tampil
        @pekerjaan = Pekerjaan.all
        render :tampilPekerjaan
    end
    
    #====================================== method tampil data pekerjaan by ID
    def tampilByID
        @pekerjaan = Pekerjaan.find_by_id(params[:id])
        if @pekerjaan.present?
            render json: {
                values: @pekerjaan,
                message: "Berhasil Menampilkan Data Pekerjaan By ID",
            }, status: 200
        else 
            render json: {
                values: "",
                message: "Data Kosong!",
            }, status: 400
        end
    end

    
    #====================================== inisialisasi awal sebelum tambah data
    #data yang dimasukkan ke database
    def pekerjaanParams
        params.permit(:kode_pekerjaan, :nama_pekerjaan)
    end
    #handler jika data tidak dimasukkan
    def pekerjaanNotFound
        render json: {
            values: {},
            message: "Data Kosong!",
        }, status: 400
    end

    #====================================== method tambah data pekerjaan
    def tambah
        @pekerjaan = Pekerjaan.new(pekerjaanParams)
        render :tambahPekerjaan
        #if @pekerjaan.save
        #    render json: {
        #        values: {},
        #        message: "Data Berhasil Ditambah",
        #    }, status: 200
        #else
        #    render json: {
        #        values: "",
        #        message: "Data Gagal Ditambah",
        #    }, status: 400
        #end
    end

    #====================================== method ubah data pekerjaan
    def ubah
        @pekerjaan = Pekerjaan.find(params[:id])
        
        if @pekerjaan.update(pekerjaanParams)
            render json: {
                values: {},
                message: "Data Berhasil Diubah",
            }, status: 200
        else
            render json: {
            message: "Data Gagal Diubah",
            }, status: 400
        end
    end

    #======================================= method hapus data pekerjaan
    def hapus
        @pekerjaan = Pekerjaan.find(params[:id])
        
        if @pekerjaan.destroy
            render json: {
                values: {},
                message: "Data Berhasil Dihapus",
            }, status: 200
        else
            render json: {
            message: "Data Gagal Dihapus",
            }, status: 400
        end
    end
end
