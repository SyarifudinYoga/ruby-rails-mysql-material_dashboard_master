Rails.application.routes.draw do
  get 'home/index'
  root 'home#index'
  #pekerjaan
  get 'pekerjaan/tampilPekerjaan' => 'pekerjaan#tampil'
  get 'pekerjaan/tampil/:id' => 'pekerjaan#tampilByID'
  post 'pekerjaan/tambah' => 'pekerjaan#tambah'
  put 'pekerjaan/ubah/:id' => 'pekerjaan#ubah'
  delete 'pekerjaan/hapus/:id' => 'pekerjaan#hapus'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
